﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sales
{
    class Program
    {
        static void Main(string[] args)
        {
            int count;
            double price;
            double rate = 0;
            double sum;
            double discount = 0;
            string state;
            string[] states = { "UT", "NV", "TX", "AL", "CA" };
            double[] rates = { 0.0685, 0.08, 0.0625, 0.04, 0.0825 };
            double[] discounts = { 0.03, 0.05, 0.07, 0.1, 0.15 };
            int[] dicountsValues = { 1000, 5000, 7000, 10000, 50000 };

            Console.WriteLine("sum = (price*count) + (price*count*rate) - (price*count*discount)");

            Console.WriteLine("Таблица налогов:");
            for (int i = 0; i < states.Length; i++)
            {
                Console.WriteLine(states[i] + ":" + rates[i] * 100 + "%");
            }

            Console.WriteLine("Таблица скидок:");
            for (int i = 0; i < discounts.Length; i++)
            {
                Console.WriteLine(dicountsValues[i] + "$:" + discounts[i] * 100 + "%");
            }

            Console.WriteLine("Введите цену:");
            price = double.Parse(Console.ReadLine());
            Console.WriteLine("Введите количество:");
            count = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите штат:");
            state = Console.ReadLine();

            for (int i = 0; i < states.Length; i++)
            {
                if (states[i] == state) { rate = rates[i]; }
            }

            Console.WriteLine("Ваш налог:" + rate * 100 + "%");

            sum = price * count;
            sum += sum * (rate);
            if (sum < 1000)
            {
                Console.WriteLine("У вас нет скидки!");
            }

            if (sum > 1000)
            {
                discount = 0.03;
                if (sum > 5000)
                {
                    discount = 0.05;
                    if (sum > 7000)
                    {
                        discount = 0.07;
                        if (sum > 1000)
                        {
                            discount = 0.1;
                            if (sum > 50000)
                            {
                                discount = 0.15;
                            }
                        }
                    }
                }
            }
            sum -= sum * (discount);
            Console.WriteLine("Ваша скидка:" + discount * 100 +"%");
            Console.WriteLine("Cумма:" + sum);
            Console.ReadKey();

            
        }
    }
}
